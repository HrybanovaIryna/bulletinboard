package bulletinBoard.utils.validator;

import bulletinBoard.dto.CommentDto;

public interface CommentValidator {

    void validate(CommentDto commentDto);
}
