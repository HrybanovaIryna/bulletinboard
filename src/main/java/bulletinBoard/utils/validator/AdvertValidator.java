package bulletinBoard.utils.validator;

import bulletinBoard.dto.AdvertDto;

public interface AdvertValidator {

    void validate(AdvertDto advertDto);

    void validateUser(Integer id);
}
