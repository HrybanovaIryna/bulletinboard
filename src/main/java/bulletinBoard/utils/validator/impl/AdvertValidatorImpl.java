package bulletinBoard.utils.validator.impl;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.exception.ValidationException;
import bulletinBoard.service.UserAuthService;
import bulletinBoard.utils.validator.AdvertValidator;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class AdvertValidatorImpl implements AdvertValidator {

    @Override
    public void validate(AdvertDto advertDto) {
        if (isNull(advertDto.getCategory()) || isNull(advertDto.getCategory().getId())) {
            throw new ValidationException("Null category in the advert", "category");
        }
    }

    @Override
    public void validateUser(Integer id) {
        if (!UserAuthService.getCurrentUserId().equals(id)) {
            throw new AdvertException("User cannot modify this advert");
        }
    }
}
