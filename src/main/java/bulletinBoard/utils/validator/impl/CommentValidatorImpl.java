package bulletinBoard.utils.validator.impl;

import bulletinBoard.dto.CommentDto;
import bulletinBoard.exception.ValidationException;
import bulletinBoard.utils.validator.CommentValidator;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

@Component
public class CommentValidatorImpl implements CommentValidator {

    @Override
    public void validate(CommentDto commentDto) {
        if (isNull(commentDto.getAdvertId())) {
            throw new ValidationException("Null advert id in the comment", "advertId");
        }
        if (isNull(commentDto.getCommentator()) || isNull(commentDto.getCommentator().getId())) {
            throw new ValidationException("Null commentator in the comment", "commentator");
        }
    }
}
