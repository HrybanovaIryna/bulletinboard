package bulletinBoard.utils.impl;

import bulletinBoard.exception.AdvertException;
import bulletinBoard.utils.FileHandler;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@Component
@Log4j2
public class FileHandlerImpl implements FileHandler {

    @Value("${file.folder}")
    private String fileFolder;

    @Override
    public String saveFile(String fileName, InputStream inputStream) {
        try {
            File folder = new File(fileFolder);
            if (!folder.exists() && folder.mkdir()) {
                throw new IOException("Cannot create folder: " + fileFolder);
            }
            File file = new File(folder, getFileName(fileName));
            Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            return file.getAbsolutePath();
        } catch (IOException e) {
            throw new AdvertException("Cannot save file: " + fileName, e);
        }
    }

    private String getFileName(String fileName) {
        return System.currentTimeMillis() + "_" + fileName;
    }

    @Override
    public void getFile(String path, OutputStream outputStream) {
        File file = new File(path);
        if (!file.exists()) {
            throw new AdvertException("File path does not exist: " + path);
        }
        try {
            Files.copy(file.toPath(), outputStream);
            outputStream.flush();
        } catch (IOException e) {
            log.error("Cannot get file: " + path, e);
        }
    }
}
