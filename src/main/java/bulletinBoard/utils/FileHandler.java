package bulletinBoard.utils;

import java.io.InputStream;
import java.io.OutputStream;

public interface FileHandler {

    String saveFile(String fileName, InputStream inputStream);

    void getFile(String path, OutputStream outputStream);
}
