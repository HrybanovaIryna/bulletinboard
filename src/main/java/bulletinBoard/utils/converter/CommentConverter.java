package bulletinBoard.utils.converter;

import bulletinBoard.dto.CommentDto;
import bulletinBoard.entity.Comment;

public interface CommentConverter {
    CommentDto convert(Comment source, CommentDto destination);
    Comment convert(CommentDto source, Comment destination);
}
