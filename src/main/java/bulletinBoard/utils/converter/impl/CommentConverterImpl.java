package bulletinBoard.utils.converter.impl;

import bulletinBoard.dto.CommentDto;
import bulletinBoard.entity.Advert;
import bulletinBoard.entity.Comment;
import bulletinBoard.entity.User;
import bulletinBoard.utils.converter.CommentConverter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
@RequiredArgsConstructor
public class CommentConverterImpl implements CommentConverter {
    private final ModelMapper modelMapper;
    private final EntityManager entityManager;

    @Override
    public CommentDto convert(Comment source, CommentDto destination) {
        modelMapper.map(source, destination);
        return destination;
    }

    @Override
    public Comment convert(CommentDto source, Comment destination) {
        modelMapper.map(source, destination);
        Advert advert = entityManager.getReference(Advert.class, source.getAdvertId());
        destination.setAdvert(advert);
        User commentator = entityManager.getReference(User.class, source.getCommentator().getId());
        destination.setCommentator(commentator);
        return destination;
    }
}
