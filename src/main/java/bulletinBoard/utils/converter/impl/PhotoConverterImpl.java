package bulletinBoard.utils.converter.impl;

import bulletinBoard.dto.PhotoDto;
import bulletinBoard.entity.Advert;
import bulletinBoard.entity.Photo;
import bulletinBoard.utils.converter.PhotoConverter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
@RequiredArgsConstructor
public class PhotoConverterImpl implements PhotoConverter {
    private final ModelMapper modelMapper;
    private final EntityManager entityManager;

    private void addAdvert(Long advertId, Photo destination) {
        Advert advert = entityManager.getReference(Advert.class, advertId);
        destination.setAdvert(advert);
    }

    @Override
    public PhotoDto convert(Photo source, PhotoDto destination) {
        modelMapper.map(source, destination);
        return destination;
    }

    @Override
    public void addAdvert(Photo photo, Long advertId) {
        addAdvert(advertId, photo);
    }
}
