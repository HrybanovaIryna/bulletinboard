package bulletinBoard.utils.converter.impl;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.entity.Advert;
import bulletinBoard.entity.Category;
import bulletinBoard.entity.User;
import bulletinBoard.service.UserAuthService;
import bulletinBoard.utils.converter.AdvertConverter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
@RequiredArgsConstructor
public class AdvertConverterImpl implements AdvertConverter {
    private final ModelMapper modelMapper;
    private final EntityManager entityManager;

    @Override
    public Advert convert(AdvertDto source, Advert destination) {
        modelMapper.map(source, destination);
        Category category = entityManager.getReference(Category.class, source.getCategory().getId());
        destination.setCategory(category);

        User user = entityManager.getReference(User.class, UserAuthService.getCurrentUserId());
        destination.setUser(user);
        return destination;
    }

    @Override
    public AdvertDto convert(Advert source, AdvertDto destination) {
        modelMapper.map(source, destination);
        return destination;
    }
}
