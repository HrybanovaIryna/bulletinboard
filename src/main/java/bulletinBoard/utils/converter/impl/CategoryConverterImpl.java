package bulletinBoard.utils.converter.impl;

import bulletinBoard.dto.CategoryDto;
import bulletinBoard.entity.Category;
import bulletinBoard.utils.converter.CategoryConverter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

import static java.util.Objects.nonNull;

@Component
@RequiredArgsConstructor
public class CategoryConverterImpl implements CategoryConverter {
    private final ModelMapper modelMapper;
    private final EntityManager entityManager;

    @Override
    public Category convert(CategoryDto source, Category destination) {
        modelMapper.map(source, destination);
        if (nonNull(source.getParentCategory())) {
            destination.setParent(entityManager.getReference(Category.class, source.getParentCategory()));
        }
        return destination;
    }

    @Override
    public CategoryDto convert(Category source, CategoryDto destination) {
        modelMapper.map(source, destination);
        return destination;
    }
}
