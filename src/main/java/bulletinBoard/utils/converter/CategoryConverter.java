package bulletinBoard.utils.converter;

import bulletinBoard.dto.CategoryDto;
import bulletinBoard.entity.Category;

public interface CategoryConverter {

    Category convert(CategoryDto source, Category destination);

    CategoryDto convert(Category source, CategoryDto destination);
}
