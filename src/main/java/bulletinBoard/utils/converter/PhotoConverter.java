package bulletinBoard.utils.converter;

import bulletinBoard.dto.PhotoDto;
import bulletinBoard.entity.Photo;

public interface PhotoConverter {

    PhotoDto convert(Photo source, PhotoDto destination);

    void addAdvert(Photo photo, Long advertId);
}
