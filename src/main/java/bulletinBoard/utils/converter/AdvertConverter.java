package bulletinBoard.utils.converter;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.entity.Advert;

public interface AdvertConverter {

    Advert convert(AdvertDto source, Advert destination);

    AdvertDto convert(Advert source, AdvertDto destination);

}
