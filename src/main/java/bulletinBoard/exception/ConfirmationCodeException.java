package bulletinBoard.exception;

public class ConfirmationCodeException extends RuntimeException {
    public ConfirmationCodeException(String massage) {
        super(massage);
    }
}
