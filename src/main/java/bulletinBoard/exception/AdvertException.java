package bulletinBoard.exception;

public class AdvertException extends RuntimeException {
    public AdvertException(String message) {
        super(message);
    }

    public AdvertException(String message, Throwable cause) {
        super(message, cause);
    }
}
