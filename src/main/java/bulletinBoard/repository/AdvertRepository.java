package bulletinBoard.repository;

import bulletinBoard.entity.Advert;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;

public interface AdvertRepository extends JpaRepository<Advert, Long> {

    Page<Advert> findAdvertByCategoryId(Integer categoryId, Pageable pageable);

    @Query("SELECT a FROM Advert a WHERE a.creationTime >= :day")
    Page<Advert> getAdvertsSinceDay(LocalDateTime day, Pageable pageable);

}
