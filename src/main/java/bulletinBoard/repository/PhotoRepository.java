package bulletinBoard.repository;

import bulletinBoard.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
    List<Photo> getPhotosByAdvertId(Long advertId);
}
