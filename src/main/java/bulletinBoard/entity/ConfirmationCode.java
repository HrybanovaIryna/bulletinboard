package bulletinBoard.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
public class ConfirmationCode {
    @Id
    @GeneratedValue
    private Integer id;
    private String activationCode;
    private int attempts;
    private LocalDateTime liveTime;
    private String email;
}
