package bulletinBoard.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@Data
public class Advert {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String text;
    private LocalDateTime creationTime;

    @ManyToOne
    private User user;

    @ManyToOne
    private Category category;
}
