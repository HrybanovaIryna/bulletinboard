package bulletinBoard.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class Comment {

    @Id
    @GeneratedValue
    private Integer id;
    @ManyToOne
    private User commentator;
    private String text;
    private LocalDateTime creationTime;

    @ManyToOne
    private Advert advert;
}
