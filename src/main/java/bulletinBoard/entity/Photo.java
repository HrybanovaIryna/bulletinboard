package bulletinBoard.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Photo {

    @Id
    @GeneratedValue
    private Long id;
    private String path;

    @ManyToOne
    private Advert advert;

}
