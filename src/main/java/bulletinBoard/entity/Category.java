package bulletinBoard.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Category {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    @ManyToOne
    private Category parent;
}
