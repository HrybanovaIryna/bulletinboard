package bulletinBoard.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@ToString(exclude = "password")
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    private String name;
    @ElementCollection
    @Enumerated
    private Set<Role> roles;
    private boolean enable;
    private boolean blocked;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
