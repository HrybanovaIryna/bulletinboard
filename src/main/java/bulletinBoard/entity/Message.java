package bulletinBoard.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
public class Message {
    @Id
    @GeneratedValue
    private Integer id;
    private String recipient;
    private String subject;
    private String text;
    private LocalDateTime sendTime;
}
