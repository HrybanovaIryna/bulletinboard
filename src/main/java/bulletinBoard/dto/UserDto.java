package bulletinBoard.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "password")
public class UserDto {
    private Integer id;
    private String name;
    private String email;
    private String password;
}
