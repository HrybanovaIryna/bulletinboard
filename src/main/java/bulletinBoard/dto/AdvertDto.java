package bulletinBoard.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AdvertDto {
    private Long id;
    private String title;
    private String text;
    private LocalDateTime creationTime;
    private UserDto user;
    private CategoryDto category;
}
