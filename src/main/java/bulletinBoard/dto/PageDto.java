package bulletinBoard.dto;

import lombok.Data;

import java.util.List;

@Data
public class PageDto<T> {
    private final List<T> objects;
    private final int totalPages;
    private final long totalElements;
}
