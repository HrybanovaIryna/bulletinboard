package bulletinBoard.dto;

import lombok.Data;

@Data
public class ChangePasswordDto {
    private String email;
    private String code;
    private String password;
}
