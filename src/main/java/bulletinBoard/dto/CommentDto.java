package bulletinBoard.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CommentDto {
    private Integer id;
    private UserDto commentator;
    private String text;
    private LocalDateTime creationTime;
    private Long advertId;

}
