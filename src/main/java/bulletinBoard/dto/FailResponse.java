package bulletinBoard.dto;

import lombok.Data;

@Data
public class FailResponse {
    private final String message;
}
