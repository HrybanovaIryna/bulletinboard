package bulletinBoard.service;

import bulletinBoard.dto.CategoryDto;
import bulletinBoard.dto.PageDto;

public interface CategoryService {

    CategoryDto addCategory(CategoryDto categoryDto);

    CategoryDto editCategory(CategoryDto categoryDto);

    boolean deleteCategory(Integer categoryId);

    PageDto<CategoryDto> getCategoryList(int page, int limit);
}
