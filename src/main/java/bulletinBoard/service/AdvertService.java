package bulletinBoard.service;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.dto.PageDto;

import java.time.LocalDateTime;

public interface AdvertService {

    AdvertDto addAdvert(AdvertDto advertDto);

    AdvertDto editAdvert(AdvertDto advertDto);

    boolean deleteAdvert(Long advertId);

    PageDto<AdvertDto> getAllAdverts(int page, int limit);

    PageDto<AdvertDto> getAdvertsSinceDay(LocalDateTime day, int page, int limit);

    PageDto<AdvertDto> getAdvertFromCategory(int categoryId, int page, int limit);
}
