package bulletinBoard.service;

import bulletinBoard.service.impl.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

public interface UserAuthService extends org.springframework.security.core.userdetails.UserDetailsService {
    static Integer getCurrentUserId()  {
        return ((CustomUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserId();
    }
}
