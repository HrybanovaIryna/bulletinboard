package bulletinBoard.service;

import bulletinBoard.dto.ChangePasswordDto;
import bulletinBoard.dto.UserDto;

public interface UserService {

    long registerNewAccount(UserDto dto);

    boolean confirmActivation(String email, String code);

    boolean sendResetCode(String email);

    boolean checkConfirmResetCode(String email, String code);

    boolean changePassword(ChangePasswordDto changePasswordDto);

    boolean blockUser(Integer userId);
}
