package bulletinBoard.service;

import bulletinBoard.dto.CommentDto;

public interface CommentService {
    CommentDto addComment(CommentDto commentDto);

    CommentDto editComment(CommentDto commentDto);

    boolean deleteComment(Integer commentId);
}
