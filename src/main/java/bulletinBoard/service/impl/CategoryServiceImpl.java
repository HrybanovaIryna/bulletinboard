package bulletinBoard.service.impl;

import bulletinBoard.dto.CategoryDto;
import bulletinBoard.dto.PageDto;
import bulletinBoard.entity.Category;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.repository.CategoryRepository;
import bulletinBoard.service.CategoryService;
import bulletinBoard.utils.converter.CategoryConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryConverter categoryConverter;

    @Override
    public CategoryDto addCategory(CategoryDto categoryDto) {
        categoryDto.setId(null);
        Category entity = new Category();
        categoryRepository.save(convert(categoryDto, entity));
        return convert(entity);
    }

    private Category convert(CategoryDto source, Category destination) {
        return categoryConverter.convert(source, destination);
    }

    @Override
    public CategoryDto editCategory(CategoryDto categoryDto) {
        Category category = findCategoryById(categoryDto.getId());
        categoryRepository.save(convert(categoryDto, category));
        return categoryDto;
    }

    @Override
    public boolean deleteCategory(Integer categoryId) {
        categoryRepository.deleteById(categoryId);
        return true;
    }

    @Override
    public PageDto<CategoryDto> getCategoryList(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        Page<Category> categories = categoryRepository.findAll(pageable);
        return new PageDto<>(convertCategories(categories.getContent()), categories.getTotalPages(), categories.getTotalElements());
    }

    private List<CategoryDto> convertCategories(List<Category> categories) {
        return categories.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private CategoryDto convert(Category source) {
        return categoryConverter.convert(source, new CategoryDto());
    }

    private Category findCategoryById(Integer id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new AdvertException("Category is not found with id " + id));
    }
}
