package bulletinBoard.service.impl;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.dto.PageDto;
import bulletinBoard.entity.Advert;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.repository.AdvertRepository;
import bulletinBoard.service.AdvertService;
import bulletinBoard.utils.converter.AdvertConverter;
import bulletinBoard.utils.validator.AdvertValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdvertServiceImpl implements AdvertService {
    private final AdvertRepository advertRepository;
    private final AdvertConverter advertConverter;
    private final AdvertValidator advertValidator;

    @Override
    public AdvertDto addAdvert(AdvertDto advertDto) {
        advertValidator.validate(advertDto);
        advertDto.setId(null);
        Advert advert = new Advert();
        convert(advertDto, advert);
        advert.setCreationTime(LocalDateTime.now());
        advertRepository.save(advert);
        return convert(advert);
    }

    @Override
    public AdvertDto editAdvert(AdvertDto advertDto) {
        advertValidator.validate(advertDto);
        Advert advert = findAdvertById(advertDto.getId());
        advertValidator.validateUser(advert.getUser().getId());
        advertRepository.save(convert(advertDto, advert));
        return convert(advert);
    }

    private Advert convert(AdvertDto source, Advert destination) {
        return advertConverter.convert(source, destination);
    }

    private Advert findAdvertById(Long id) {
        return advertRepository.findById(id)
                .orElseThrow(() -> new AdvertException("Advert is not found with id " + id));
    }

    @Override
    public boolean deleteAdvert(Long advertId) {
        Advert advert = findAdvertById(advertId);
        advertValidator.validateUser(advert.getUser().getId());
        advertRepository.delete(advert);
        return true;
    }

    @Override
    public PageDto<AdvertDto> getAllAdverts(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        Page<Advert> adverts = advertRepository.findAll(pageable);
        return new PageDto<>(convertAdverts(adverts.getContent()), adverts.getTotalPages(), adverts.getTotalElements());
    }

    private List<AdvertDto> convertAdverts(List<Advert> adverts) {
        return adverts.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    @Override
    public PageDto<AdvertDto> getAdvertsSinceDay(LocalDateTime day, int page, int limit) {
        Page<Advert> adverts = advertRepository.getAdvertsSinceDay(day, PageRequest.of(page, limit));
        return new PageDto<>(convertAdverts(adverts.getContent()), adverts.getTotalPages(), adverts.getTotalElements());
    }

    @Override
    public PageDto<AdvertDto> getAdvertFromCategory(int categoryId, int page, int limit) {
        Page<Advert> adverts = advertRepository.findAdvertByCategoryId(categoryId, PageRequest.of(page, limit));
        List<AdvertDto> collect = convertAdverts(adverts.getContent());
        return new PageDto<>(collect, adverts.getTotalPages(), adverts.getTotalElements());
    }

    private AdvertDto convert(Advert source) {
        return advertConverter.convert(source, new AdvertDto());
    }
}
