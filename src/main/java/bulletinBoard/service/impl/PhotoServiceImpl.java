package bulletinBoard.service.impl;

import bulletinBoard.dto.PhotoDto;
import bulletinBoard.entity.Photo;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.repository.PhotoRepository;
import bulletinBoard.service.PhotoService;
import bulletinBoard.utils.FileHandler;
import bulletinBoard.utils.converter.PhotoConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PhotoServiceImpl implements PhotoService {
    private final PhotoRepository photoRepository;
    private final PhotoConverter photoConverter;
    private final FileHandler fileHandler;

    @Override
    public List<PhotoDto> getPhotos(Long advertId) {
        List<Photo> photos = photoRepository.getPhotosByAdvertId(advertId);
        return convertPhotos(photos);
    }

    private List<PhotoDto> convertPhotos(List<Photo> photos) {
        return photos.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private PhotoDto convert(Photo source) {
        return photoConverter.convert(source, new PhotoDto());
    }

    @Override
    public boolean deletePhoto(Long id) {
        photoRepository.deleteById(id);
        return true;
    }

    @Override
    public PhotoDto addPhoto(String fileName, Long advertId, InputStream inputStream) {
        Photo photo = new Photo();
        photoConverter.addAdvert(photo, advertId);
        photo.setPath(fileHandler.saveFile(fileName, inputStream));
        photoRepository.save(photo);
        return convert(photo);
    }

    @Override
    public void getPhoto(Long id, OutputStream outputStream) {
        Photo photo = photoRepository.findById(id)
                .orElseThrow(() -> new AdvertException("Cannot find photo with id: " + id));
        fileHandler.getFile(photo.getPath(), outputStream);
    }
}
