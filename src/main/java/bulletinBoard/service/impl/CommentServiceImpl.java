package bulletinBoard.service.impl;

import bulletinBoard.dto.CommentDto;
import bulletinBoard.entity.Comment;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.repository.CommentRepository;
import bulletinBoard.service.CommentService;
import bulletinBoard.utils.converter.CommentConverter;
import bulletinBoard.utils.validator.CommentValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final CommentConverter commentConverter;
    private final CommentValidator commentValidator;

    @Override
    public CommentDto addComment(CommentDto commentDto) {
        commentValidator.validate(commentDto);
        commentDto.setId(null);
        Comment comment = new Comment();
        convert(commentDto, comment);
        comment.setCreationTime(LocalDateTime.now());
        Comment entity = commentRepository.save(comment);
        return convert(entity);
    }

    private void convert(CommentDto source, Comment destination) {
        commentConverter.convert(source, destination);
    }

    @Override
    public CommentDto editComment(CommentDto commentDto) {
        commentValidator.validate(commentDto);
        Comment comment = findCommentById(commentDto.getId());
        convert(commentDto, comment);
        commentRepository.save(comment);
        return convert(comment);
    }

    private CommentDto convert(Comment comment) {
        return commentConverter.convert(comment, new CommentDto());
    }

    @Override
    public boolean deleteComment(Integer commentId) {
        commentRepository.deleteById(commentId);
        return true;
    }

    private Comment findCommentById(Integer id) {
        return commentRepository.findById(id)
                .orElseThrow(() -> new AdvertException("Comment is not found with id " + id));
    }
}
