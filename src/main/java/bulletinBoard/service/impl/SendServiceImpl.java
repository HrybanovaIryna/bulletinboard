package bulletinBoard.service.impl;

import bulletinBoard.entity.Message;
import bulletinBoard.repository.MessageRepository;
import bulletinBoard.service.SendService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Data
@RequiredArgsConstructor
public class SendServiceImpl implements SendService {
    private final JavaMailSender mailSender;
    private final MessageRepository messageRepository;
    @Value("{$sender}")
    private String sender;

    @Override
    public void sendEmail(String recipient, String subject, String text) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(sender);
        mailMessage.setTo(recipient);
        mailMessage.setSubject(subject);
        mailMessage.setText(text);
        mailSender.send(mailMessage);

        Message message = new Message();
        message.setRecipient(recipient);
        message.setSubject(subject);
        message.setText(text);
        message.setSendTime(LocalDateTime.now());
        messageRepository.save(message);
    }
}
