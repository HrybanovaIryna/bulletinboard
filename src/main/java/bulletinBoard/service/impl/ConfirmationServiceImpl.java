package bulletinBoard.service.impl;

import bulletinBoard.entity.ConfirmationCode;
import bulletinBoard.exception.ConfirmationCodeException;
import bulletinBoard.repository.ConfirmationCodeRepository;
import bulletinBoard.service.ConfirmationService;
import bulletinBoard.service.SendService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class ConfirmationServiceImpl implements ConfirmationService {
    private final SendService sendService;
    private final ConfirmationCodeRepository confirmationCodeRepository;
    private static final int MAX_ATTEMPTS = 5;
    private static final int CODE_LIVE_TIME_DAYS = 1;
    private static final int SCHEDULED_RATE = 30 * 60 * 1000;


    @Override
    public void sendCode(String email, String url) {
        checkEmail(email);
        ConfirmationCode confirmationCode = confirmationCodeRepository.findByEmail(email)
                .orElseGet(() -> generateCode(email));

        sendService.sendEmail(email, "Activate your account",
                "To complete registration follow a link: <a href=\"" + generateConfirmationLink(email,
                        confirmationCode.getActivationCode(), url) + "\">Зареєструватися</a>");
    }

    private ConfirmationCode generateCode(String email) {
        ConfirmationCode confirmationCode = new ConfirmationCode();
        confirmationCode.setActivationCode(UUID.randomUUID().toString());
        confirmationCode.setLiveTime(LocalDateTime.now().plusDays(CODE_LIVE_TIME_DAYS));
        confirmationCode.setEmail(email);
        confirmationCodeRepository.save(confirmationCode);
        return confirmationCode;
    }

    private String generateConfirmationLink(String email, String confirmationCode, String url) {
        return url + "?email=" + email + "&code=" + confirmationCode;
    }

    private void checkEmail(String email) {
        if (isNull(email) || email.isEmpty()) {
            throw new ConfirmationCodeException("Incorrect email " + email);
        }
    }

    @Override
    public boolean checkCode(String email, String code) {
        checkEmail(email);
        ConfirmationCode confirmationCode = confirmationCodeRepository.findByEmail(email)
                .orElseThrow(() -> new ConfirmationCodeException("Incorrect code"));

        if (confirmationCode.getActivationCode().equals(code)) {
            confirmationCodeRepository.delete(confirmationCode);
            return true;
        }

        confirmationCode.setAttempts(confirmationCode.getAttempts() + 1);
        if (confirmationCode.getAttempts() > MAX_ATTEMPTS) {
            confirmationCodeRepository.delete(confirmationCode);
            throw new ConfirmationCodeException("Incorrect code");
        }
        confirmationCodeRepository.save(confirmationCode);
        return false;
    }

    @Override
    public boolean checkCodePresence(String email, String code) {
        checkEmail(email);
        return confirmationCodeRepository.findByEmail(email).isPresent();
    }

    @Transactional
    @Scheduled(fixedRate = SCHEDULED_RATE)
    public void deleteCode() {
        confirmationCodeRepository.deleteCode();
    }
}
