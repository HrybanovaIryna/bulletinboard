package bulletinBoard.service.impl;

import bulletinBoard.dto.ChangePasswordDto;
import bulletinBoard.dto.UserDto;
import bulletinBoard.entity.Role;
import bulletinBoard.entity.User;
import bulletinBoard.exception.UserException;
import bulletinBoard.repository.UserRepository;
import bulletinBoard.service.ConfirmationService;
import bulletinBoard.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ConfirmationService confirmationService;
    private final PasswordEncoder passwordEncoder;
    @Value("http://localhost:8080/user/confirm")
    private String confirmationUrl;
    @Value("http://localhost:8080/user/confirmResetCode")
    private String confirmationResetCodeUrl;

    @Override
    @Transactional
    public long registerNewAccount(UserDto dto) {
        if (emailRegistered(dto)) {
            throw new UserException("User with email " + dto.getEmail() + " is already registered.");
        }
        checkLoginAndPassword(dto.getEmail(), dto.getPassword());

        User user = new User(dto.getEmail(), dto.getPassword());
        user.setName(dto.getName());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        userRepository.save(user);
        confirmationService.sendCode(user.getEmail(), confirmationUrl);
        return user.getId();
    }

    private boolean emailRegistered(UserDto dto) {
        return userRepository.findByEmail(dto.getEmail()).isPresent();
    }

    private User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserException("User not found with login " + email));
    }

    private void checkLoginAndPassword(String email, String password) {
        if (isEmpty(email)) {
            throw new UserException("Incorrect email" + email);
        }

        if (isEmpty(password)) {
            throw new UserException("Incorrect password " + password);
        }
    }

    private boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    @Override
    public boolean confirmActivation(String email, String code) {
        if (confirmationService.checkCode(email, code)) {
            User user = getUserByEmail(email);
            user.setEnable(true);
            userRepository.save(user);
        }
        return true;
    }

    @Override
    public boolean sendResetCode(String email) {
        getUserByEmail(email);
        confirmationService.sendCode(email, confirmationResetCodeUrl);
        return true;
    }

    @Override
    public boolean checkConfirmResetCode(String email, String code) {
        return confirmationService.checkCodePresence(email, code);
    }

    @Override
    public boolean changePassword(ChangePasswordDto changePasswordDto) {
        if (checkConfirmResetCode(changePasswordDto.getEmail(), changePasswordDto.getCode())) {
            User user = getUserByEmail(changePasswordDto.getEmail());
            user.setPassword(passwordEncoder.encode(changePasswordDto.getPassword()));
            userRepository.save(user);
        }
        return true;
    }

    @Override
    public boolean blockUser(Integer userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserException("User not found with id: " + userId));
        user.setBlocked(true);
        return true;
    }
}
