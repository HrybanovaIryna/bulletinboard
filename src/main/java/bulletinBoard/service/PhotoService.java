package bulletinBoard.service;

import bulletinBoard.dto.PhotoDto;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface PhotoService {

    List<PhotoDto> getPhotos(Long advertId);

    boolean deletePhoto(Long id);

    PhotoDto addPhoto(String fileName, Long advertId, InputStream inputStream);

    void getPhoto(Long id, OutputStream outputStream);
}
