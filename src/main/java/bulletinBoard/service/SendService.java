package bulletinBoard.service;

public interface SendService {
    void sendEmail(String recipient, String subject, String text);
}
