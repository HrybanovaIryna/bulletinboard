package bulletinBoard.service;

public interface ConfirmationService {

    void sendCode(String email, String url);

    boolean checkCode(String email, String code);

    boolean checkCodePresence(String email, String code);
}
