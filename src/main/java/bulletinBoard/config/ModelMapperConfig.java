package bulletinBoard.config;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.dto.CategoryDto;
import bulletinBoard.dto.CommentDto;
import bulletinBoard.dto.UserDto;
import bulletinBoard.entity.Advert;
import bulletinBoard.entity.Category;
import bulletinBoard.entity.Comment;
import bulletinBoard.entity.User;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Objects.isNull;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        addCategoryTypeMap(modelMapper);
        addAdvertTypeMap(modelMapper);
        addCommentTypeMap(modelMapper);
        addUserTypeMap(modelMapper);
        return modelMapper;
    }

    private void addAdvertTypeMap(ModelMapper modelMapper) {
        modelMapper.createTypeMap(AdvertDto.class, Advert.class)
                .addMappings(mapper -> mapper.skip(Advert::setCreationTime));
    }

    private void addCategoryTypeMap(ModelMapper modelMapper) {
        Converter<Category, Integer> categoryToInteger =
                el -> isNull(el.getSource()) ? null : el.getSource().getId();
        modelMapper.createTypeMap(Category.class, CategoryDto.class)
                .addMappings(mapper -> mapper.using(categoryToInteger)
                        .map(Category::getParent, CategoryDto::setParentCategory));
    }

    private void addCommentTypeMap(ModelMapper modelMapper) {
        modelMapper.getConfiguration().setImplicitMappingEnabled(false);
        modelMapper.typeMap(CommentDto.class, Comment.class)
                .addMappings(mapper -> mapper.skip(Comment::setCreationTime))
                .addMappings(mapper -> mapper.skip(Comment::setCommentator))
                .addMappings(mapper -> mapper.skip(Comment::setAdvert))
                .implicitMappings();
        modelMapper.getConfiguration().setImplicitMappingEnabled(true);


        Converter<Advert, Long> advertToLong =
                el -> isNull(el.getSource()) ? null : el.getSource().getId();
        modelMapper.createTypeMap(Comment.class, CommentDto.class)
                .addMappings(mapper -> mapper.using(advertToLong)
                        .map(Comment::getAdvert, CommentDto::setAdvertId));
    }

    private void addUserTypeMap(ModelMapper modelMapper) {
        modelMapper.createTypeMap(User.class, UserDto.class)
                .addMappings(mapper -> mapper.skip(UserDto::setPassword));
    }
}
