package bulletinBoard.config;

import bulletinBoard.service.UserAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String[] SWAGGER_RESOURCES = {
            "/",
            "/csrf",
            "/v2/api-docs",
            "/swagger-resources/configuration/ui",
            "/configuration/ui",
            "/swagger-resources",
            "/swagger-resources/configuration/security",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

    private final UserAuthService userAuthService;
    private final PasswordEncoder passwordEncoder;

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userAuthService);
        authProvider.setPasswordEncoder(passwordEncoder);
        return authProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .httpBasic()
                .and()
                .formLogin().disable()
                .authorizeRequests()
                .antMatchers(SWAGGER_RESOURCES).permitAll()
                .antMatchers("/category/**", "/user/block").hasRole("ADMIN")
                .antMatchers("/comment/**", "/advert/**", "/photo/**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/user/registration",
                        "/user/confirm",
                        "/user/sendResetCode",
                        "/user/confirmResetCode",
                        "/user/changePassword",
                        "/user/login").permitAll()
                .anyRequest().authenticated();
    }
}
