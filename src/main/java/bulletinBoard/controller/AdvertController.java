package bulletinBoard.controller;

import bulletinBoard.dto.AdvertDto;
import bulletinBoard.dto.PageDto;
import bulletinBoard.service.AdvertService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/advert")
@RequiredArgsConstructor
@Log4j2
public class AdvertController {
    private final AdvertService advertService;

    @PostMapping
    public AdvertDto addAdvert(@RequestBody AdvertDto dto) {
        log.info("Add advert request: " + dto);
        AdvertDto response = advertService.addAdvert(dto);
        log.info("Add advert response: " + response);
        return response;
    }

    @PutMapping
    public AdvertDto editAdvert(@RequestBody AdvertDto dto) {
        log.info("Edit advert request: " + dto);
        AdvertDto response = advertService.editAdvert(dto);
        log.info("Edit advert response: " + response);
        return response;
    }

    @DeleteMapping
    public boolean deleteAdvert(@RequestParam Long advertId) {
        log.info("Delete advert request: " + advertId);
        boolean response = advertService.deleteAdvert(advertId);
        log.info("Delete advert response: " + response);
        return response;
    }

    @GetMapping("/byCategory")
    public PageDto<AdvertDto> getAdvertFromCategory(@RequestParam Integer categoryId, @RequestParam int page, @RequestParam int limit) {
        log.info("Get advert by category request: categoryId: " + categoryId + " page: " + page + " limit: " + limit);
        PageDto<AdvertDto> response = advertService.getAdvertFromCategory(categoryId, page, limit);
        log.info("Get advert by category response: " + response);
        return response;
    }

    @GetMapping("/all")
    public PageDto<AdvertDto> getAllAdverts(@RequestParam int page, @RequestParam int limit) {
        log.info("Get all adverts request: page: " + page + " limit: " + limit);
        PageDto<AdvertDto> response = advertService.getAllAdverts(page, limit);
        log.info("Get all adverts response: " + response);
        return response;
    }

    @GetMapping("/sinceDay")
    public PageDto<AdvertDto> getNewAdverts(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime day, @RequestParam int page, @RequestParam int limit) {
        log.info("Get new adverts request: day " + day + " page: " + page + " limit: " + limit);
        PageDto<AdvertDto> response = advertService.getAdvertsSinceDay(day, page, limit);
        log.info("Get new adverts response: " + response);
        return response;
    }
}
