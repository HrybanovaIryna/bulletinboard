package bulletinBoard.controller;

import bulletinBoard.dto.CategoryDto;
import bulletinBoard.dto.PageDto;
import bulletinBoard.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
@Log4j2
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping
    public CategoryDto addCategory(@RequestBody CategoryDto categoryDto) {
        log.info("Add category request: " + categoryDto);
        CategoryDto response = categoryService.addCategory(categoryDto);
        log.info("Add category response: " + response);
        return response;
    }

    @PutMapping
    public CategoryDto editCategory(@RequestBody CategoryDto categoryDto) {
        log.info("Edit category request: " + categoryDto);
        CategoryDto response = categoryService.editCategory(categoryDto);
        log.info("Edit category response: " + response);
        return response;
    }

    @DeleteMapping
    public boolean deleteCategory(@RequestParam Integer categoryId) {
        log.info("Delete category request: " + categoryId);
        boolean response = categoryService.deleteCategory(categoryId);
        log.info("Delete category response: " + response);
        return response;
    }

    @GetMapping("/all")
    public PageDto<CategoryDto> getCategoryList(@RequestParam int page, @RequestParam int limit) {
        log.info("Get category request: page: " + page + " limit: " + limit);
        PageDto<CategoryDto> response = categoryService.getCategoryList(page, limit);
        log.info("Get category response: " + response);
        return response;
    }
}
