package bulletinBoard.controller;

import bulletinBoard.dto.FailResponse;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.exception.ValidationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Log4j2
public class ExceptionController {
    @ExceptionHandler(AdvertException.class)
    public FailResponse processAdvertException(AdvertException e) {
        log.error("Was caught AdvertException", e);
        return new FailResponse(e.getMessage());
    }

    @ExceptionHandler(ValidationException.class)
    public FailResponse processValidationException(ValidationException e) {
        log.error("Was caught ValidationException. wrong field: " + e.getField(), e);
        return new FailResponse("Wrong field: " + e.getField());
    }

    @ExceptionHandler(Exception.class)
    public FailResponse processException(Exception e) {
        log.error("Was caught Exception", e);
        return new FailResponse(e.getMessage());
    }
}
