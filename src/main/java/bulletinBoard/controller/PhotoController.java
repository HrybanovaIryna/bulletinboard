package bulletinBoard.controller;

import bulletinBoard.dto.PhotoDto;
import bulletinBoard.exception.AdvertException;
import bulletinBoard.service.PhotoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/photo")
@Log4j2
public class PhotoController {
    private final PhotoService photoService;

    @GetMapping("/byAdvert")
    public List<PhotoDto> getPhotos(@RequestParam Long advertId) {
        log.info("Get photos by advert request:" + advertId);
        List<PhotoDto> response = photoService.getPhotos(advertId);
        log.info("Get photos by advert response: " + response);
        return response;
    }

    @GetMapping
    public void getPhoto(@RequestParam Long id, HttpServletResponse response) {
        log.info("Get photo by id: " + id);
        try (OutputStream outputStream = response.getOutputStream()) {
            photoService.getPhoto(id, outputStream);
            response.setContentType("image/jpg");
            log.info("Photo successfully downloaded");
        } catch (IOException e) {
            throw new AdvertException("Cannot download file " + id, e);
        }
    }

    @DeleteMapping
    boolean deletePhoto(@RequestParam Long id) {
        log.info("Delete photo request " + id);
        boolean response = photoService.deletePhoto(id);
        log.info("Delete photo response " + response);
        return response;
    }

    @PostMapping
    PhotoDto addPhoto(@RequestParam String fileName, @RequestParam Long advertId, @RequestParam MultipartFile file) {
        log.info("Add photo request, fileName: " + fileName + ", advertId: " + advertId);
        try (InputStream inputStream = file.getInputStream()) {
            PhotoDto response = photoService.addPhoto(fileName, advertId, inputStream);
            log.info("Add photo response " + response);
            return response;
        } catch (IOException e) {
            throw new AdvertException("Cannot upload file " + fileName, e);
        }

    }
}
