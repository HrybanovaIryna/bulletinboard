package bulletinBoard.controller;

import bulletinBoard.dto.ChangePasswordDto;
import bulletinBoard.dto.UserDto;
import bulletinBoard.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Log4j2
public class UserController {
    private final UserService userService;

    @PostMapping("/registration")
    public long registration(@RequestBody UserDto userDto) {
        log.info("User registration request: " + userDto);
        long response = userService.registerNewAccount(userDto);
        log.info("User registration response: " + response);
        return response;
    }

    @GetMapping("/confirm")
    public boolean confirmActivation(@RequestParam String email, @RequestParam String code) {
        log.info("User confirm request: email: " + email + ", code: " + code);
        boolean response = userService.confirmActivation(email, code);
        log.info("User confirm response: " + response);
        return response;
    }

    @GetMapping("/sendResetCode")
    public boolean sendResetCode(@RequestParam String email){
        log.info("User send reset code request: " + email);
        boolean response = userService.sendResetCode(email);
        log.info("User send reset code response : " + response);
        return response;
    }

    @GetMapping("/confirmResetCode")
    public boolean confirmResetCode(@RequestParam String email, @RequestParam String code) {
        log.info("User confirm reset code request, email: " + email + ", code: " + code);
        boolean response = userService.checkConfirmResetCode(email, code);
        log.info("User confirm reset code response: " + response);
        return response;
    }

    @PostMapping("/changePassword")
    public boolean changePassword(@RequestBody ChangePasswordDto changePasswordDto) {
        log.info("User change password request: " + changePasswordDto);
        boolean response = userService.changePassword(changePasswordDto);
        log.info("User change password response: " + response);
        return response;
    }

    @PutMapping("/block")
    public boolean blockUser(@RequestParam int userId) {
        log.info("Block user request, id: " + userId);
        boolean response = userService.blockUser(userId);
        log.info("Block user response: " + response);
        return response;
    }
}
