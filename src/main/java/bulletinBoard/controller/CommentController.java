package bulletinBoard.controller;

import bulletinBoard.dto.CommentDto;
import bulletinBoard.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comments")
@RequiredArgsConstructor
@Log4j2
public class CommentController {
    private final CommentService commentService;

    @PostMapping
    public CommentDto addComment(@RequestBody CommentDto commentDto) {
        log.info("Add comment request: " + commentDto);
        CommentDto response = commentService.addComment(commentDto);
        log.info("Add comment response: " + response);
        return response;
    }

    @PutMapping
    public CommentDto editComment(@RequestBody CommentDto commentDto) {
        log.info("Edit comment request: " + commentDto);
        CommentDto response = commentService.editComment(commentDto);
        log.info("Edit comment response: " + response);
        return response;
    }

    @DeleteMapping
    public boolean deleteComment(@RequestParam Integer commentId) {
        log.info("Delete comment request: " + commentId);
        boolean response = commentService.deleteComment(commentId);
        log.info("Delete comment response: " + response);
        return response;
    }
}
